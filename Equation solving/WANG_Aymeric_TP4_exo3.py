# -*- coding: utf-8 -*-
"""
Éditeur Spider

Ceci est un script temporaire.
"""

"""
#------------------------#------------------------
# TP 4 Exercice 3 : Système non linéaire
#------------------------#------------------------
"""


"""
#------------------------#------------------------
# Library imports
#------------------------#------------------------
"""

import numpy as np
import matplotlib.pyplot as plt


"""
#------------------------#------------------------
# Functions
#------------------------#------------------------
"""


def Newton(x0,y0,error):
    """returns two lists X and Y of coordinates calculated with Newton's 
    method. I set down a counter of a 100 steps of calculus, arbitrarily.
    xi & yi [floats]: variables of this problem calculated at step i
    F [array (2,1)]: vector of the result of problem given x and y
    Jf [array (2,2)]: Jacobian matrix of the problem
    Vi [array (2,1)]: vector (xi,yi) calculated at step i
    err [float]: error admitted in the numerical resolution"""
    
    X,Y = [x0],[y0]
    V0 = np.array([[x0],[y0]])
    Jf_xy0 = Jf(x0,y0)
    F_xy0 = F(x0,y0)
    
    dV0 = Gauss(Jf_xy0,-F_xy0)
    V1 = V0 + dV0
    
    X.append(V1[0,0])
    Y.append(V1[1,0])
    count = 1
    while Norme2(F(V1[0,0],V1[1,0]))>error and count<10**2:
        Jf_xy1 = Jf(V1[0,0],V1[1,0])
        F_xy1 = F(V1[0,0],V1[1,0])
        
        dV1 = Gauss(Jf_xy1,-F_xy1)
        V1 = V1 + dV1
        
        X.append(V1[0,0])
        Y.append(V1[1,0])
        
        count+=1
    
    return(X,Y)
    
def Jf(x,y):
    """returns the Jacobian matrix for the exercice's problem given x and y
    Jf_xy [array(2,2)]: Jacobian matrix given x and y"""
    
    Jf_xy = np.array([[np.exp(x)-2,-1],[2*(x-1),2*(y-1)]])
    return(Jf_xy)

def F(x,y):
    """returns the estimate result for the exercice's problem given x and y
    F_xy [array(2,1)]: vector of the estimated result of the problem"""
    
    F_xy = np.array([[np.exp(x)-2*x-y],[(x-1)**2+(y-1)**2-4]])
    return(F_xy)

def Triangularisation(A,b):
    """triangularize a system given a matrix A and un vector b in a 
    system Ax=b where x is an unknown vector """
    n = A.shape[0]
    M = np.zeros((n,n+1))
    M[:,:n] = A[:,:]
    M[:,n] = b[:,0]
    
    for k in range(n):
        for i in range(k+1,n):
            p = k+1
            while M[k, k] == 0:
                M[k, :], M[p, :] = M[p, :], M[k, :]
                p += 1
                if p >= n:
                    print("Le système n'est triangularisable")
                    return None
            c = M[i, k]/M[k, k]
            for j in range(k, n+1):
                M[i, j] -= c*M[k, j]
    return M

def Gauss(A,b):
    """solve the linear problem AX=b and returns X
    x & y [floats]: variables of this problem
    F [array (2,1)]: vector of the result of problem given x and y
    Jf [array (2,2)]: Jacobian matrix of the problem
    V1 [array (2,1)]: vector with useful data for solving the system"""
    
    M = Triangularisation(A, b)
    n = A.shape[0]
    
    X = np.zeros((n, 1))
    X[n-1, 0] = M[n-1, n]/M[n-1, n-1]
    for i in range(n-2, -1, -1):
        s = 0 # s stands for sum
        for j in range(i+1, n):
            s += M[i, j]*X[j]
        X[i, 0] = (M[i, n] - s)/M[i, i]
    return X


def Norme2(X):
    """return the norm 2 of a vector X
    n [int]: dimension of the vector X
    X [array (n,1)]: vector of interest"""
    
    Norm = 0 # initializing the variable with the norm calculus 
    for elt in X : # elt stands for element
        Norm += elt[0]**2
        
    return Norm**0.5


"""
#------------------------#------------------------
# Program
#------------------------#------------------------
"""

# Méthode analytique

x1 = np.linspace(-3,3,100) # list of axises for the first function
x2 = np.linspace(-1,3,100) # list of axises for circle
y1 = np.exp(x1)-2*x1 # list of ordinates for the first function
y21 = 1-(3+2*x2-x2**2)**0.5 # list of ordinates for the first part of the circle
y22 = 1+(3+2*x2-x2**2)**0.5 # list of ordinates for the second part of the circle


# Méthode de Newton
x_newt1,y_newt1=Newton(4,3,10**(-3))  
x_newt2,y_newt2=Newton(3,4,10**(-3))


plt.figure(1)
plt.axis("equal")
plt.title("Méthode de Newton avec pour points de départ (4;3) et (3;4)")
plt.xlabel(r"x$\in[-3;3]$")
plt.plot(x1,y1,"b",label="Solution analytique y(x)")
plt.plot(x2,y21,"r",label="Cercle")
plt.plot(x2,y22,"r")
plt.plot(x_newt1,y_newt1,"c:o",label="Point de départ (4;3)")
plt.plot(x_newt2,y_newt2,"b:o",label="Point de départ (3;4)")
plt.legend()
plt.show()