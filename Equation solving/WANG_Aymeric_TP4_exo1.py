# -*- coding: utf-8 -*-

"""
#------------------------#------------------------
__author__      = "Aymeric Wang"
__copyright__   = "Copyright (C) 2017, Aymeric Wang"
#------------------------#------------------------
"""

"""
#------------------------#------------------------
# TP 4 Exercice 1 : Méthode numérique
#------------------------#------------------------
"""


"""
#------------------------#------------------------
# Library imports
#------------------------#------------------------
"""

import numpy as np
import matplotlib.pyplot as plt


"""
#------------------------#------------------------
# Functions
#------------------------#------------------------
"""

def Euler(yn,zn,h):
    """returns a vector Vn1=(yn1,zN1) useful for solving the exercice's system
    yn & yn1 [floats]: stand respectively for yn and yn+1
    zn & zn1 [floats]: stand respectively for zn and zn+1
    h [float]: resolution chosen in the Euler method 
    Vn [array (2,1)]: vector with useful data for solving the system"""
    Vn = np.array([[yn],[zn]])
    A = np.array([[1,h],[-h,2*h+1]]) # matrix determined when solving the Euler system
    Vn1 = np.dot(A,Vn)
    return(Vn1)


def f(yn,zn):
    return(-yn+2*zn)


def EulerModif(yn,zn,h):
    """return zn+1 (zn1) and yn+1 (yn1) in the Adams-Moulton 2 method given 
    yn, zn and h in the numerical resolution """
    zn1 = zn + h*f(yn+(h/2),zn+(h/2)*f(yn,zn))
    yn1 = yn + (h/2)*(zn1+zn)
    return(yn1,zn1)


"""
#------------------------#------------------------
# Program
#------------------------#------------------------
"""

# Méthode analytique

x_analyt = [i/100 for i in range(101)] # list of axises
y_analyt = [-(e-2)*np.exp(e) for e in x_analyt] # list of ordinates for y(x)
z_analyt = [-(e-1)*np.exp(e) for e in x_analyt] # list of ordinates for y'(x)
# N.B. : e stands for element


# Méthode d'Euler

print("\nMéthode d'Euler")
n=int(input("Quel pas (n!=0) souhaitez-vous pour calculer la solution ? n = ? "))
while n==0 :
    n=int(input("Veuillez prendre n!=0. n = ? "))

h=1/n
x_euler=[i/n for i in range(n+1)] # list of axises
y_euler=[2] # initializing list of ordinates
z_euler=[1]

# determining y_euler
yk=y_euler[0]
zk=z_euler[0]
for k in range(1,n+1):
    Vk=Euler(yk,zk,h)
    yk,zk = Vk[0,0],Vk[1,0]
    y_euler.append(yk)
    z_euler.append(zk)


# Méthode d'Euler modifiée
h=1/n
x_eulerm=[i/n for i in range(n+1)] # list of axises
y_eulerm=[2] # initializing list of ordinates for y(x)
z_eulerm=[1] # same but for y'(x)

for k in range(1,n+1):
    yk,zk = EulerModif(y_eulerm[-1],z_eulerm[-1],h)
    y_eulerm.append(yk)
    z_eulerm.append(zk)


plt.figure(1)
plt.title("Comparaison solution analytique/méthode d\'Euler à "+str(n)+" itérations")
plt.xlabel(r"x$\in[0;1]$")
plt.plot(x_analyt,y_analyt,"b",label="Solution analytique y(x)")
plt.plot(x_analyt,z_analyt,"g",label="Solution analytique y'(x)")
plt.plot(x_euler,y_euler,"b:o",label="Méthode d'Euler pour y(x)")
plt.plot(x_euler,z_euler,"g:o",label="Méthode d'Euler pour y'(x)")
plt.legend()
plt.show()

plt.figure(2)
plt.title("Comparaison solution analytique/méthode d\'Euler modifiée à "+str(n)+" itérations")
plt.xlabel(r"x$\in[0;1]$")
plt.plot(x_analyt,y_analyt,"b",label="Solution analytique y(x)")
plt.plot(x_analyt,z_analyt,"g",label="Solution analytique y'(x)")
plt.plot(x_eulerm,y_eulerm,"b:o",label="Méthode d'Euler modifiée pour y(x)")
plt.plot(x_eulerm,z_eulerm,"g:o",label="Méthode d'Euler modifiée pour y'(x)")
plt.legend()
plt.show()