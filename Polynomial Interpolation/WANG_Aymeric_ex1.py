# -*- coding: utf-8 -*-
"""
Éditeur Spider

Ceci est un script temporaire.
"""

"""
#------------------------#------------------------
# TP 3 Exercice 1 : Interpolation polynomiale pts
#------------------------#------------------------
"""


"""
#------------------------#------------------------
# Library imports
#------------------------#------------------------
"""

import numpy as np
import urllib.request
import matplotlib.pyplot as plt


"""
#------------------------#------------------------
# Functions
#------------------------#------------------------
"""

def LectureFichier(dim,nom_fichier):
    """returns an array with coordinates data from a .txt file
    dim [int]: number of points to be extracted
    nom_fichier [str]: url path toward the .txt file """
    
    fichier = urllib.request.urlopen(nom_fichier) # open a file online
    A=np.zeros((dim,3),float) # list used for extracting useful data
    
    ligne = fichier.readline() # reads one line
    
    for i in range(0, dim):
        ligne = fichier.readline()
        chaine=ligne.decode( "utf-8" ) # converts byte data into readable text
        
        # begining extraction of useful data placed in A
        number='' # number placed in A before being converted in a float
        colonne=0 # column of A in which the number will be inserted
        B=[] # i founded easier to work with lists instead of str
        for e in chaine :
            B.append(e)
        B.remove('\r')
        B.remove('\n')
        B.append(' ') # if not added, errors will occur in the following lines
        
        for k in range(len(B)):
            if B[k]!=' ':
                number+=B[k]
            if B[k]==' ' and number!='':
                A[i,colonne]=float(number)
                number=''
                colonne+=1
    
    fichier.close()
    return(A)



def Vandermonde(X,n):
    """returns the Vandermonde matrix from a set of axises
    X [list/array]: list of axises 
    n [int]: length of the matrix
    Mat_Vand [array]: the Vandermonde matrix returned"""
    
    Mat_Vand=np.zeros((n,n),float)
    for i in range(len(X)):
        for j in range(n):
            Mat_Vand[i,j]=X[i]**j
    return(Mat_Vand)



"""
#------------------------#------------------------
# Program
#------------------------#------------------------
"""

# ask for the number of dots to interpolate
dim=int(input("Quel est le nombre de points que vous souhaitez tracer ?\nOn doit avoir 2<=N<=10 : N = ? "))

while dim<2 or dim>10 :
    dim=int(input("Veuillez prendre 2<=N<=10 : N = ? "))

nom_fichier = "http://www.lsis.org/louruding/points_interpolatation/points_NB["+str(dim)+"]"
A=LectureFichier(dim,nom_fichier)

X=A[:,1] # axises to be interpolated
Y=A[:,2] # ordinates to be interpolated
n=len(X)

Mat_Vand=Vandermonde(X,n)

# i chose to transpose the Y array to have a much "natural" matrix product
Yt=np.zeros((n,1),float)
for i in range(n):
    Yt[i,0]=Y[i]


# solving the linear system with coefficients of the interpolation polynomial
Coeffs_inter=np.linalg.solve(Mat_Vand,Yt)

x_pol=np.linspace(min(X),max(X),100)
y_pol=Coeffs_inter[0,0]*(x_pol**0)
for i in range(1,n):
    y_pol+=Coeffs_inter[i,0]*(x_pol**i)


# graphical results
plt.figure()
plt.grid()
plt.scatter(X,Y,c="black",marker="o")
plt.plot(x_pol,y_pol,c="blue")
plt.title("interpolation_pts["+str(dim)+"]")
plt.show()