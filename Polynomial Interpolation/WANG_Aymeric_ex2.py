# -*- coding: utf-8 -*-
"""
Éditeur Spider

Ceci est un script temporaire.
"""

"""
#------------------------#------------------------
# TP 3 Exercice 2 : Approximation polynomiale pts
#------------------------#------------------------
"""


"""
#------------------------#------------------------
# Library imports
#------------------------#------------------------
"""

import numpy as np
import urllib.request
import matplotlib.pyplot as plt


"""
#------------------------#------------------------
# Functions
#------------------------#------------------------
"""

def LectureFichier(nom_fichier):
    """returns an array with coordinates data from a .txt file
    nom_fichier [str]: path toward the .txt file """
    
    fichier = urllib.request.urlopen(nom_fichier) # open a file on line
    A=np.zeros((50,3),float) # list used for extracting useful data
    
    ligne = fichier.readline() # reads one line
    
    for i in range(0, 50):
        ligne = fichier.readline()
        chaine=ligne.decode( "utf-8" ) # converts byte data into readable text
        
        # begining extraction of useful data placed in A
        number='' # number placed in A before being converted in a float
        colonne=0 # column of A in which the number will be inserted
        B=[] # i founded easier to work with lists instead of str
        for e in chaine :
            B.append(e)
        B.remove('\r')
        B.remove('\n')
        B.append(' ') # if not added, errors will occur in the following lines
        
        for k in range(len(B)):
            if B[k]!=' ':
                number+=B[k]
            if B[k]==' ' and number!='':
                A[i,colonne]=float(number)
                number=''
                colonne+=1
    
    fichier.close()
    return(A)



def SystMatriciel(X,degree,n):
    """returns the matrix used in approximation linear system from a set of axises
    X [list/array]: list of axises
    n [int]: number of columns of the matrix
    A [array]: the returned matrix closed to a Vandermonde matrix1"""
    
    A=np.zeros((n,degree+1),float)
    for i in range(n):
        for j in range(degree+1):
            A[i,j]=X[i]**j
    return(A)



"""
#------------------------#------------------------
# Program
#------------------------#------------------------
"""

# ask for the number of dots to interpolate
dim=int(input("Quel numéro (N°) de fichier de points souhaitez-vous tracer ?\n On doit avoir 1<=N°<=10 : N° = ? "))

while dim<1 or dim>10 :
    dim=int(input("Veuillez prendre 1<=N°<=10 : N° = ? "))

degree=int(input("Quel degré (d) imposez-vous au polynome ?\nOn doit avoir 1<=d<=10 : d = ? "))

while degree<1 or degree>10 :
    degree=int(input("Veuillez prendre 1<=d<=10 : d = ? "))

nom_fichier = "http://www.lsis.org/louruding/points_approximation/points_type["+str(dim)+"]"
B=LectureFichier(nom_fichier)

X=B[:,1] # axises to be interpolated
Y=B[:,2] # ordinates to be interpolated
n=len(X)

# i chose to transpose the Y array to have a much "natural" matrix product
Yt=np.zeros((n,1),float)
for i in range(n):
    Yt[i,0]=Y[i]

A=SystMatriciel(X,degree,n)
At=A.T # transposition of the matrix A
C=np.dot(At,A)
Y2=np.dot(At,Yt)

# solving the linear system with coefficients of the approximation polynomial
Coeffs_approx=np.linalg.solve(C,Y2)

x_pol=np.linspace(min(X),max(X),100)
y_pol=Coeffs_approx[0,0]*(x_pol**0)
for i in range(1,degree+1):
    y_pol+=Coeffs_approx[i,0]*(x_pol**i)


# graphical results
plt.figure()
plt.grid()
plt.scatter(X,Y,c="black",marker="o")
plt.plot(x_pol,y_pol,c="blue")
title="approximation_pts["+str(dim)+"]\ndegree = "+str(degree)
plt.title(title)
plt.show()