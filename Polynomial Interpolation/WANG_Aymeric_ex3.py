# -*- coding: utf-8 -*-
"""
Éditeur Spider

Ceci est un script temporaire.
"""

"""
#------------------------#------------------------
# TP 3 Exercice 3 : Résolution numérique
#------------------------#------------------------
"""


"""
#------------------------#------------------------
# Library imports
#------------------------#------------------------
"""

import numpy as np
import matplotlib.pyplot as plt


"""
#------------------------#------------------------
# Functions
#------------------------#------------------------
"""

def f(x,y):
    """returns the result of the Cauchy problem given x and y(x)"""
    return(x**2-2*y+1)

def phiRK4(xn,yn,h):
    """return the result of the function phi in the Runge-Kutta 4 method given 
    xn, yn and h in the numerical resolution """
    K1 = f(xn,yn)
    K2 = f(xn+h/2,yn+h*K1/2)
    K3 = f(xn+h/2,yn+h*K2/2)
    K4 = f(xn+h,yn+h*K3)
    return((K1+2*K2+2*K3+K4)/6)

def yn1AM2(xn,xn1,yn,h):
    """return yn+1 (yn1) in the Adams-Moulhon 2 method given xn, yn, xn+1 
    (xn1) and h in the numerical resolution """
    return((yn+(h/2)*(2+xn**2-2*yn+xn1**2))/(1+h))

"""
#------------------------#------------------------
# Program
#------------------------#------------------------
"""

# Solution analytique

x_analyt = np.linspace(0,1,500) # list of axises for analytical solution
y_analyt = [0]*len(x_analyt) # list of ordinates for analytical solution

for k in range(len(x_analyt)):
    y_analyt[k] = (3 + np.exp(-2*x_analyt[k]))/4 + ((x_analyt[k]**2)-x_analyt[k])/2



# Méthodes numériques

h=1/4 # pace for all numerical solutions
x_num = [i*h for i in range(int(1/h+1))]


# Méthode d'Euler

Euler=[1]+[0]*int(1/h)
Euler_err=[np.abs(Euler[0]-y_analyt[0])]

for i in range(1,len(Euler)):
    Euler[i]=Euler[i-1]+h*f(x_num[i-1],Euler[i-1])
    Euler_err.append(np.abs((Euler[i]-y_analyt[int(i*500*h-1)])))



# Méthode d'Euler modifiée

Euler_modif=[1]+[0]*int(1/h)
Euler_modif_err=[np.abs(Euler_modif[0]-y_analyt[0])]

for i in range(1,len(x_num)):
    Euler_modif[i]=Euler_modif[i-1]+h*f(x_num[i-1]+h/2,Euler_modif[i-1]+(h/2)*f(x_num[i-1],Euler_modif[i-1]))
    Euler_modif_err.append(np.abs((Euler_modif[i]-y_analyt[int(i*500*h-1)])))



# Méthode de Runge-Kutta d'ordre 4

RK4=[1]+[0]*int(1/h)
RK4_err=[np.abs(RK4[0]-y_analyt[0])]

for i in range(1,len(x_num)):
    RK4[i]=RK4[i-1]+h*phiRK4(x_num[i-1],RK4[i-1],h)
    RK4_err.append(np.abs((RK4[i]-y_analyt[int(i*500*h-1)])))


# Méthode d'Adams-Bashforth d'ordre 2

AB2=[1]+[Euler_modif[1]]+[0]*int(1/h-1) # i chose y1 from the modified Euler method
AB2_err=[np.abs(AB2[0]-y_analyt[0]),np.abs(AB2[1]-y_analyt[int(500*h-1)])]

for i in range(2,len(x_num)):
    AB2[i]=AB2[i-1]+(h/2)*(3*f(x_num[i-1],AB2[i-1])-f(x_num[i-2],AB2[i-2]))
    AB2_err.append(np.abs((AB2[i]-y_analyt[int(i*500*h-1)])))


# Méthode d'Adams-Moulton d'ordre 2

AM2=[1]+[0]*int(1/h)
AM2_err=[np.abs(AM2[0]-y_analyt[0])]

for i in range(1,len(x_num)):
    AM2[i]=yn1AM2(x_num[i-1],x_num[i],AM2[i-1],h)
    AM2_err.append(np.abs((AM2[i]-y_analyt[int(i*500*h-1)])))



# Affichage graphique des solutions numériques et analytique

plt.figure(1)
plt.title(r"Résultats des méthodes numériques $\left(h=\frac{1}{4}\right)$:")
plt.xlabel("x")
plt.ylabel("y(x)")
plt.grid()
plt.plot(x_analyt,y_analyt,c='black',label='Solution analytique')
plt.plot(x_num,Euler,"--o",c='purple',label='Méthode d\'Euler')
plt.plot(x_num,Euler_modif,"--o",c='red',label='Méthode d\'Euler modifiée')
plt.plot(x_num,RK4,"--o",c='green',label='Méthode de Runge-Kutta d\'ordre 4')
plt.plot(x_num,AB2,"--o",c='blue',label='Méthode d\'Adams-Bashforth d\'ordre 2')
plt.plot(x_num,AM2,"--o",c='orange',label='Méthode d\'Adams-Moulton d\'ordre 2')
plt.legend()
plt.show()



# Affichage graphique des erreurs des méthodes numériques en console

plt.figure(2)
plt.title(r"Erreurs des méthodes numériques $\left(h=\frac{1}{4}\right)$:")
plt.xlabel("x")
plt.ylabel("y(x)")
plt.grid()
plt.plot(x_num,Euler_err,"--o",c='purple',label='Méthode d\'Euler')
plt.plot(x_num,Euler_modif_err,"--o",c='red',label='Méthode d\'Euler modifiée')
plt.plot(x_num,RK4_err,"--o",c='green',label='Méthode de Runge-Kutta d\'ordre 4')
plt.plot(x_num,AB2_err,"--o",c='blue',label='Méthode d\'Adams-Bashforth d\'ordre 2')
plt.plot(x_num,AM2_err,"--o",c='orange',label='Méthode d\'Adams-Moulton d\'ordre 2')
plt.legend()
plt.show()


"""On constate que la méthode de Runge-Kutta d'ordre 4 est la plus précise et
 c'est pourquoi elle est plus utilisée."""
