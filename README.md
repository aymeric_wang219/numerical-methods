# Hello!

This repository is itself divided into two other repositories.
1. **Equation solving** aims to analyze efficiency of numerical resolving simple non-linear equations (Euler, Newton and Runge-Kutta)
2. **Polynomial Interpolation** aims to assess how polynomial interpolation is done, regarding how CAD softwares finite element simulations work when dealing discrete points of study.

# Context & Disclaimer

This is a code I did during computing classes at Arts et Metiers institute of Technology. DO NOT copy and paste the entirety of this code if you are an ENSAM student, you might get caught and I do not want it to cause any harm to you. Thank you.